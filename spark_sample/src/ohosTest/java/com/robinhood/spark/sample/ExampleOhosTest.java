/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robinhood.spark.sample;

import com.robinhood.os_spark.utils.PxUtil;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * ExampleOhosTest
 */
public class ExampleOhosTest {
    /**
     * testBundleName
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.robinhood.spark.sample", actualBundleName);
    }

    /**
     * testFormatNum1
     */
    @Test
    public void testFormatNum1() {
        int num = 10;
        String result = PxUtil.formatNum(num);
        Assert.assertNotNull(result);
        Assert.assertEquals("10.0", result);
    }

    /**
     * testFormatNum2
     */
    @Test
    public void testFormatNum2() {
        float num = 10.543f;
        String result = PxUtil.formatNum(num);
        Assert.assertNotNull(result);
        Assert.assertEquals("10.54", result);
    }
}