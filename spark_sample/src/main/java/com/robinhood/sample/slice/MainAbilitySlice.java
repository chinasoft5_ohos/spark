package com.robinhood.sample.slice;

import com.robinhood.os_spark.SparkAdapter;
import com.robinhood.os_spark.SparkView;
import com.robinhood.os_spark.animation.LineSparkAnimator;
import com.robinhood.os_spark.animation.MorphSparkAnimator;
import com.robinhood.spark.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.service.WindowManager;

import java.security.SecureRandom;
import java.util.Random;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private RandomizedAdapter mAdapter;
    private SparkView mSparkView;
    private Text mScrubInfoTextView;
    private Button mButton;
    private Button mButton2;
    private Button mButton3;

    @Override
    public void onStart(Intent intent) {
        //沉浸式状态栏
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initListeners();
    }

    private void initListeners() {
        mSparkView = (SparkView) findComponentById(ResourceTable.Id_spark);
        mScrubInfoTextView = (Text) findComponentById(ResourceTable.Id_scrub_info_textview);
        mButton = (Button) findComponentById(ResourceTable.Id_button1);
        mButton2 = (Button) findComponentById(ResourceTable.Id_button2);
        mButton3 = (Button) findComponentById(ResourceTable.Id_button3);
        Button button5 = (Button) findComponentById(ResourceTable.Id_button4);
        button5.setClickedListener(this);
        Checkbox button4 = (Checkbox) findComponentById(ResourceTable.Id_check);
        mButton.setClickedListener(this);
        mButton2.setClickedListener(this);
        mButton3.setClickedListener(this);

        button4.setCheckedStateChangedListener((absButton, b) -> {
            if (b)
                mSparkView.setFillType(SparkView.FillType.DOWN);
            else
                mSparkView.setFillType(SparkView.FillType.NONE);
        });
        mAdapter = new RandomizedAdapter();
        mSparkView.setAdapter(mAdapter);
        mSparkView.setScrubListener(value -> {
            if (value == null) {
                mScrubInfoTextView.setText(getString(ResourceTable.String_scrub_empty));
            } else {
                mScrubInfoTextView.setText(getString(ResourceTable.String_scrub_format, value));
            }
        });


    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {

        switch (component.getId()) {
            case ResourceTable.Id_button1:
                seleteItem(mButton);
                LineSparkAnimator lineSparkAnimator = new LineSparkAnimator();
                lineSparkAnimator.setDurationInternal(2500);
                lineSparkAnimator.setCurveTypeInternal(Animator.CurveType.ACCELERATE_DECELERATE);

                mSparkView.setSparkAnimator(lineSparkAnimator);
                mAdapter.randomize();
                break;
            case ResourceTable.Id_button2:
                seleteItem(mButton2);
                MorphSparkAnimator morphSparkAnimator = new MorphSparkAnimator();
                morphSparkAnimator.setDurationInternal(800);
                morphSparkAnimator.setCurveTypeInternal(Animator.CurveType.ACCELERATE_DECELERATE);
                mSparkView.setSparkAnimator(morphSparkAnimator);
                mAdapter.randomize();
                break;
            case ResourceTable.Id_button3:
                seleteItem(mButton3);
                mSparkView.setSparkAnimator(null);
                mAdapter.randomize();
                break;
            case ResourceTable.Id_button4:
                mAdapter.randomize();
                break;
        }


    }


    public static class RandomizedAdapter extends SparkAdapter {
        private final float[] yData;
        private final SecureRandom random;

        public RandomizedAdapter() {
            random = new SecureRandom();
            yData = new float[50];
            randomize();
        }

        public void randomize() {
            for (int i = 0, count = yData.length; i < count; i++) {
                yData[i] = random.nextFloat();
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return yData.length;
        }

        @Override
        public Object getItem(int index) {
            return yData[index];
        }

        @Override
        public float getY(int index) {
            return yData[index];
        }
    }

   public void seleteItem(Button button){
       ShapeElement element = new ShapeElement();
       element.setRgbColor(RgbPalette.WHITE);
       mButton.setBackground(element);
       mButton2.setBackground(element);
       mButton3.setBackground(element);
       ShapeElement element1 = new ShapeElement();
       element1.setRgbColor(RgbPalette.GREEN);
       button.setBackground(element1);

   }
}
