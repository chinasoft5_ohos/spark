package com.robinhood.os_spark.animation;


import com.robinhood.os_spark.SparkView;
import ohos.agp.animation.Animator;

/**
 *  This interface is for animate SparkView when it changes
 */
public interface SparkAnimator {

    /**
     * Returns an Animator that performs the desired animation. Must call
     * {@link SparkView#setAnimationPath} for each animation frame.
     *
     * See {@link LineSparkAnimator} and {@link MorphSparkAnimator} for examples.
     *
     * @param sparkView The SparkView object
     * @return Animator
     */
    Animator getAnimation(final SparkView sparkView);
}
