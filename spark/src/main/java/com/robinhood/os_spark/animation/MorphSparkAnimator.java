package com.robinhood.os_spark.animation;

import com.robinhood.os_spark.SparkView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.render.Path;

import java.util.List;

/**
 * Animates each point vertically from the previous position to the current position.
 */
public class MorphSparkAnimator extends Animator implements SparkAnimator {

    private final AnimatorValue animator;
    private List<Float> oldYPoints;
    private final Path animationPath;

    public MorphSparkAnimator() {
//        animator = AnimatorValue.ofFloat(0, 1);
        animator = new AnimatorValue();
        animationPath = new Path();
    }

    @Override
    public Animator getAnimation(final SparkView sparkView) {

        final List<Float> xPoints = sparkView.getXPoints();
        final List<Float> yPoints = sparkView.getYPoints();
        if (xPoints.isEmpty() || yPoints.isEmpty()) {
            return null;
        }

            animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    animationPath.reset();

                    float step;
                    float y, oldY;
                    int size = xPoints.size();
                    for (int count = 0; count < size; count++) {

                        // get oldY, can be 0 (zero) if current points are larger
                        oldY = oldYPoints != null && oldYPoints.size() > count ? oldYPoints.get(count) : 0f;

                        step = (float) ((double)yPoints.get(count) - (double)oldY);
                        y = (float) (((double)step * (double)v) + (double)oldY);

                        if (count == 0) {
                            animationPath.moveTo(xPoints.get(count), y);
                        } else {
                            animationPath.lineTo(xPoints.get(count), y);
                        }

                    }

                    // set the updated path for the animation
                    sparkView.setAnimationPath(animationPath);
                }
            });

        animator.setStateChangedListener(new StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                oldYPoints = yPoints;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        return animator;
    }

    @Override
    public long getDelay() {
        return animator.getDelay();
    }

    @Override
    public void setDelayInternal(long startDelay) {
        animator.setDelay(startDelay);
    }

    @Override
    public void setDurationInternal(long duration) {
            animator.setDuration(duration);
    }

    @Override
    public long getDuration() {
        return animator.getDuration();
    }

    @Override
    public void setCurveTypeInternal(int curveType) {
        animator.setCurveType(curveType);
    }

    @Override
    public boolean isRunning() {
        return animator.isRunning();
    }
}
