/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robinhood.os_spark.utils;


import java.text.DecimalFormat;

/**
 * PxUtil
 */
public class PxUtil {

    /**
     * 格式化数字(保留一位小数)
     *
     * @param num 数字
     * @return string
     */
    public static String formatNum(int num) {
        DecimalFormat format = new DecimalFormat("0.0");
        return format.format(num);
    }

    /**
     * 格式化数字(保留两位小数)
     *
     * @param num 数字
     * @return string
     */
    public static String formatNum(float num) {
        if (((int) num * 1000) == (int) (num * 1000)) {
            return String.valueOf((int) num);
        }
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(num);
    }
}
